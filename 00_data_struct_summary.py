
# tuple
tuple_x = (2, 4, 8, 16, 32)

# list
list_x = [2, 4, 8, 16, 32]


print("Tuples and list")
print(30*"=")

print(type(tuple_x))
print(tuple_x)
print(type(list_x))
print(list_x)

print(f"Length of the tuple: {len(list_x)} elements")
print(f"Length of the list : {len(list_x)} elements")

print()
print("=" * 30)
print()
#====================================================================================
# Print elements of a data structure - see lesson 04-control-flow
print()
print("="*30) 
# Tuple elements
print("Tuple elements:")
for elem in tuple_x:
    print(elem, end=" ")
print("\nEnd Tuple elements")

# List elements
print("List elements:")
for elem in list_x:
    print(elem, end=" ")
print("\nEnd List elements")
