# 03 Data Structures



## Name

Python Data Structures

## Description

Data structures in python
| Data structure | Explanation | Example |
|:---------------|:-------------------------------------------------|:-------------------------------------:|
| tuple          | a list that can't be modified                    | ("John", 23, 87)                      |
| list           | a list that can be modified                      | ["john", 23, 87] => ["John", 23, 97]  |
| set            | a list of unique values. Has no duplicate values | {"John", "Mary"}                      |
| dictionary     | a key - value pair list                          | {"name":"john", "age":23, "grade":87} |


Example of python 

```python
tuple_x = (2, 4, 8, 16, 32)

list_x = [2, 4, 8, 16, 32]

print("Tuples and list")
print(30*"+")

print(type(tuple_x))
print(tuple_x)
print(type(list_x))
print(list_x)

print(f"Length of the tuple: {len(list_x)} elements")
print(f"Length of the list : {len(list_x)} elements")
```

Tuple cannot have  elements modified, added, removed  elements
the methods can be visualised with: 

List can have elements added removed. In short lists can be modified

   - dir(tuple)  count
   - dir(list)   append extend insert remove pop clear sort reverse copy count

### Methods for tuples and lists

   - name_tuple.*count(value)* - will count how many times the value appear in the tuple
   - name_list.*count(value)* - will count *number of times* the **value** appear in the list 
   - name_tuple.*append(value)* - will give an error. **A tuple cannot be modified** 
   - name_list.*append(value)* - will append at the end of the list **value**
   - name_list.*extend(iterable)* - will extend the list with the values from an iterable
   - name_list.*insert(index, value)* - will insert a **value** into a list at specified **index**
   - name_list.*remove(value)* - will remove first occurence of the **value** from the list
   - name_list.*pop([index])* - will remove *the last* element or the element at the *index* position and return it.  
   - name_list.index(value,[start], [end]) -  return the **index** of **value** - first occurence. The **start** and the **end** are optional and limit (slice the list)
   - name_list.*clear()* - will remove all **values** from the list
   - name_list.*sort(\*, key=None, reverse=False)* - will sort the list ascending. reverse=True sorting is descending
   - name_list.*reverse()* - will reverse elements of the list
   - name_list.*copy()* - will rturn a shallow copy of the list

```python
# count

ex_tuple = (1, 3 , 1, 2, 16, 32)

ex_list = [1, 3, 4, 1, 16, 32]
ex_list2 = [128, 256, 512, 1024]


ex_tuple.count(1)   # will count value 1 two times
ex_list.count(1)    # will count value 1 two times 

# append

ex_tuple.append(64) # will cause an error
ex_list.append(64)  # will append value 64 to the end of the list

# extend

extend_list = ['a', 'b']
extend_list2 = ['c', 'd']
extend_list.extend(ex_list2)    # ['a', 'b', 'c', 'd']

# insert

insert_list = ['Tom', 'Molly']
insert_list.insert(1, 'Brad')   # ['Tom' 'Brad', 'Molly']

# remove

## Remove the first occurence in the list

remove_list = ['Tom', 'Brad', 'Molly' 'Brad']
remove_list.remove('Brad')      # ['Tom', 'Molly', 'Brad']

# pop

list_pop1 = ['Tom', 'Brad', 'Molly']
list_pop2 = ['Tom', 'Brad', 'Molly']

list1.pop()     # Molly
list2.pop(0)    # Tom

# index 

list_index1 = ['Molly', 'Brad', 'Tom']
list_index2 = ['Bobby', 'Edward', 'Tim' 'Bobby']

list_index1.index('Brad')           # 1
list_index2.index('Bobby', 1, 3)    # 3

# clear

list_clear = [3, 5, 7, 11]
list_clear.clear()  # []

# sort 


##!!! ATTENTION  sort modify the original list 
## the examples are aplied on original list **unmodified** 

## After the first sort a second sort to the sorted list can have undesired effects if the list is a list of strings

list_sort = [4, 2, 6, 1]
list_names_sort = ['Bobby', 'John', 'Claude', 'bob']

list_sort.sort()            # [1, 2, 4, 6]
list.sort(reverse=True)     # [6, 4, 2, 1]

list_names_sort.sort()                                      # ['Bobby', 'Claude', 'John', 'bob']
list_names_sort.sort(reverse=True)                          # ['bob', 'John', 'Claude', 'Bobby']
list_names_sort.sort(key=str.lower, reverse=True)           # ['John', 'Claude', 'Bobby', 'bob']


# reverse 

list_rev =  [1, 4, 2, 5]
list_rev.reverse()          # [5, 2, 4, 1] 

# copy

list_a = ['a', 'c', 'd', 'b']
list_b = list_a

print(list_b)               # ['a', 'c', 'd', 'b']

list_b[0] = 'x'

print(list_b)               # ['x', 'c', 'd', 'b']          
print(list_a)               # ['x', 'c', 'd', 'b']          

# list_b = list_a  means  location in memory of list_a is the same as list_b. 
# Modification of the list_b modify the value in the memory the reference points to. 
# Having the same memory address both lists are modified no matter what list is modified

# if is need a copy

list_a = ['a', 'c', 'd', 'b']
list_b.copy(list_a)

print(list_b)               # ['a', 'c', 'd', 'b']          
print(list_a)               # ['a', 'c', 'd', 'b']          

list_b[0] = 'x'

print(list_b)               # ['x', 'c', 'd', 'b']          
print(list_a)               # ['a', 'c', 'd', 'b']          

```


## Usage

- Clone repository

```bash
git clone https://gitlab.com/python-beginner-tutorial/03-data-structures.git
cd 03-data-structures
```
- Run the file with python

```bash
python data_struct.py
```

