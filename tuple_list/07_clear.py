print("Tuple - list difference\n")
# tuple cannot have  elements modified, added, removed elements
# the methods can be visualised with: 
# - dir(tuple)  count 
# - dir(list)   count append extend insert remove pop clear sort reverse copy count 

# TUPLE IS AN IMUTABLE FIXED SIZE DATA STRUCTURE
# LIST IS AN MUTABLE VARIABLE SIZE DATA STRUCTURE - 

# In list but not in tuple
## avaible data can be modified, 
## new data can be added, 
## existent data removed

# clear
print("clear\n")

list_dogs = ['Spike', 'Nobi', 'Blakie'] 
print(f"list_dogs = {list_dogs}")
list_dogs.clear()       # []
print("="*30)
print(f"list_dogs: {list_dogs}")
