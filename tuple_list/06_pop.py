print("Tuple - list difference\n")
# tuple cannot have  elements modified, added, removed elements
# the methods can be visualised with: 
# - dir(tuple)  count 
# - dir(list)   count append extend insert remove pop clear sort reverse copy count 

# TUPLE IS AN IMUTABLE FIXED SIZE DATA STRUCTURE
# LIST IS AN MUTABLE VARIABLE SIZE DATA STRUCTURE - 

# In list but not in tuple
## avaible data can be modified, 
## new data can be added, 
## existent data removed

# pop
print("pop\n")

list_dogs1 = ['Spike', 'Nobi', 'Blakie'] 
list_dogs2 = ['Bethowen', 'Thor', 'Kuro']
print(f"First List of dogs is {list_dogs1}")
print(f"Second List of dogs is {list_dogs2}")

dog_name1 = list_dogs1.pop()    #  Blackie
dog_name2 = list_dogs2.pop(1)   # Thor
print("="*30)
print(f"Dog 'poped' from list 1 {dog_name1}")
print(f"Dog 'poped' from list 2 {dog_name2}")
