print("Tuple - list difference\n")
# tuple cannot have  elements modified, added, removed elements
# the methods can be visualised with: 
# - dir(tuple)  count 
# - dir(list)   count append extend insert remove pop clear sort reverse copy count 

# TUPLE IS AN IMUTABLE FIXED SIZE DATA STRUCTURE
# LIST IS AN MUTABLE VARIABLE SIZE DATA STRUCTURE - 

# In list but not in tuple
## avaible data can be modified, 
## new data can be added, 
## existent data removed

# copy
print("copy\n")

list_dogs = ['Spike', 'Nobi', 'Blakie'] 
list_names = ['bob', 'Terry', 'John', 'Blake', 'Bob', 'Mark']
list_numbers = [7, 4, 8, 5, 2, 1, 9, 13,]

print(f"list_dogs = {list_dogs}")
print(f"list_names = {list_names}")
print(f"list_numbers = {list_numbers}")
print("*"*30)

list_dogs_2 = list_dogs
list_names_2 = list_names
list_numbers_2 = list_numbers

print("Copy List 1 to List 2")
print(f"list_dogs_2 = {list_dogs_2}")
print(f"list_names_2 = {list_names_2}")
print(f"list_numbers_2 = {list_numbers_2}")
print("*"*30)
print("Modifies List 2")
list_dogs_2[1] = 'Bella'
list_names_2[0] = 'Bobby'
list_numbers_2[2] = 3

print("*"*30)
print("list_dogs_2[1] ='Bella'\nlist_names_2[0] = 'Bobby'\nlist_numbers_2[2] = 3\n")
print(f"list_dogs = {list_dogs}\nlist_dogs_2 = {list_dogs_2}")
print(f"list_names = {list_names}\nlist_names_2 = {list_names_2}")
print(f"list_numbers = {list_numbers}\nlist_numbers_2 = {list_numbers_2}")
print("*"*30)
print("This modifies list one as well because list_2 sets are refrences to list_1\nIn memory both variables points the same location")
print("*"*30)
print("Shallow copy")
list_dogs_copy = list_dogs.copy()
list_names_copy = list_names.copy()
list_numbers_copy = list_numbers.copy()

print(f"list_dogs = {list_dogs}\tlist_dogs_copy = {list_dogs_copy}")
print(f"list_names = {list_names}\tlist_names_copy = {list_names_copy}")
print(f"list_numbers = {list_numbers}\tlist_numbers_copy = {list_numbers_copy}")
print("*"*30)
list_dogs_copy[0] = 'Donald'
list_names_copy[1] = 'Donald'
list_numbers_copy[2] = 69

print(f"list_dogs = {list_dogs}\tlist_dogs_copy = {list_dogs_copy}")
print(f"list_names = {list_names}\tlist_names_copy = {list_names_copy}")
print(f"list_numbers = {list_numbers}\tlist_numbers_copy = {list_numbers_copy}")
