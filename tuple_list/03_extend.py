print("Tuple - list difference\n")
# tuple cannot have  elements modified, added, removed elements
# the methods can be visualised with: 
# - dir(tuple)  count 
# - dir(list)   count append extend insert remove pop clear sort reverse copy count 

# TUPLE IS AN IMUTABLE FIXED SIZE DATA STRUCTURE
# LIST IS AN MUTABLE VARIABLE SIZE DATA STRUCTURE - 

# In list but not in tuple
## avaible data can be modified, 
## new data can be added, 
## existent data removed

# extend
print("extend\n")

list_1_extend = ['a', 'b', 'c']
list_2_extend = ['x', 'y', 'z']

print(list_1_extend)
print(list_2_extend)

# list TWO is added to the end of FIRST LIST 
list_1_extend.extend(list_2_extend) # ['a', 'b', 'c', 'x', 'y', 'z']

print(f"\nList 1 extended with list 2 {list_1_extend}\n")

