print("Tuple - list difference\n")
# tuple cannot have  elements modified, added, removed elements
# the methods can be visualised with: 
# - dir(tuple)  count 
# - dir(list)   count append extend insert remove pop clear sort reverse copy count 

# TUPLE IS AN IMUTABLE FIXED SIZE DATA STRUCTURE
# LIST IS AN MUTABLE VARIABLE SIZE DATA STRUCTURE - 

# In list but not in tuple
## avaible data can be modified, 
## new data can be added, 
## existent data removed

ex_tuple = (1, 3 , 4 , 1, 16, 32)
ex_list = [1, 3, 4, 1, 16, 32]

print(f"ex_tuple = {ex_tuple}")
print(f"ex_list = {ex_list}")

num_2_tuple = ex_tuple.count(1)
num_2_list = ex_list.count(1)

print("\n# count\n")
print(f"1 is counted {num_2_tuple} times in the tuple") 
print(f"1 is counted {num_2_list} times in the list") 

