print("Tuple - list difference\n")
# tuple cannot have  elements modified, added, removed elements
# the methods can be visualised with: 
# - dir(tuple)  count 
# - dir(list)   count append extend insert remove pop clear sort reverse copy count 

# TUPLE IS AN IMUTABLE FIXED SIZE DATA STRUCTURE
# LIST IS AN MUTABLE VARIABLE SIZE DATA STRUCTURE - 

# In list but not in tuple
## avaible data can be modified, 
## new data can be added, 
## existent data removed

# reverse 
print("rverse\n")

list_dogs = ['Spike', 'Nobi', 'Blakie'] 
list_names = ['bob', 'Terry', 'John', 'Blake', 'Bob', 'Mark']
list_numbers = [7, 4, 8, 5, 2, 1, 9, 13,]

print(f"list_dogs = {list_dogs}")
print(f"list_names = {list_names}")
print(f"list_numbers = {list_numbers}")
print("*"*30)
print("List reversed")
list_dogs.reverse()
list_names.reverse()
list_numbers.reverse()
print(list_dogs)
print(list_names)
print(list_numbers)
