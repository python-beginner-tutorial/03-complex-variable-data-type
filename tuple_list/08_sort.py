print("Tuple - list difference\n")
# tuple cannot have  elements modified, added, removed elements
# the methods can be visualised with: 
# - dir(tuple)  count 
# - dir(list)   count append extend insert remove pop clear sort reverse copy count 

# TUPLE IS AN IMUTABLE FIXED SIZE DATA STRUCTURE
# LIST IS AN MUTABLE VARIABLE SIZE DATA STRUCTURE - 

# In list but not in tuple
## avaible data can be modified, 
## new data can be added, 
## existent data removed

# sort 
print("sort\n")

list_dogs = ['Spike', 'Nobi', 'Blakie'] 
list_names = ['bob', 'Terry', 'John', 'Blake', 'Bob', 'Mark']
list_numbers = [7, 4, 8, 5, 2, 1, 9, 13,]

print(f"list_dogs = {list_dogs}")
print(f"list_names = {list_names}")
print(f"list_numbers = {list_numbers}")
print("*"*30)
print("List sorted default - ascending")
list_dogs.sort()
list_names.sort()
list_numbers.sort()
print(list_dogs)
print(list_names)
print(list_numbers)

print("="*30)

list_dogs2 = ['Spike', 'Nobi', 'Blakie'] 
list_names2 = ['bob', 'Terry', 'John', 'Blake', 'Bob', 'Mark']
list_numbers2 = [7, 4, 8, 5, 2, 1, 9, 13,]
print(f"list_dogs2 = {list_dogs2}")
print(f"list_names2 = {list_names2}")
print(f"list_numbers2 = {list_numbers2}")
print("*"*30)
print("List sorted reverse - dscending")
list_dogs2.sort(reverse=True)
list_names2.sort(reverse=True)
list_numbers2.sort(reverse=True)    # list_numbers2 is a list og integers not strings
print(list_dogs2)
print(list_names2)
print(list_numbers2)
print("*"*30)
print("List sorted - case insesitive")
list_dogs2.sort(key=str.lower)
list_names2.sort(key=str.lower)
list_numbers2.sort()
print(list_dogs2)
print(list_names2)
print(list_numbers2)
print("*"*30)
print("List sorted reverse - case insensitive")
list_dogs2.sort(key=str.lower, reverse=True)
list_names2.sort(key=str.lower, reverse=True)
list_numbers2.sort(reverse=True)    # list_numbers2 is a list og integers not strings
print(list_dogs2)
print(list_names2)
print(list_numbers2)
