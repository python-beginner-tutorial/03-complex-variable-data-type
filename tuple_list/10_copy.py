print("Tuple - list difference\n")
# tuple cannot have  elements modified, added, removed elements
# the methods can be visualised with: 
# - dir(tuple)  count 
# - dir(list)   count append extend insert remove pop clear sort reverse copy count 

# TUPLE IS AN IMUTABLE FIXED SIZE DATA STRUCTURE
# LIST IS AN MUTABLE VARIABLE SIZE DATA STRUCTURE - 

# In list but not in tuple
## avaible data can be modified, 
## new data can be added, 
## existent data removed

# index
print("index\n")

list_dogs = ['Spike', 'Nobi', 'Blakie', 'Nobi'] 

print("list_indx = ['  0  ', '  1 ', '  2   ', '  3 ']")
print(f"list_dogs = {list_dogs}")
print("*"*30)

dog_index_nobi = list_dogs.index('Nobi')          # 1
dog_index_nobi2 = list_dogs.index('Nobi', 3, 4)    # 3

print(f"Index 'Nobi': {dog_index_nobi}")
print(f"Index second 'Nobi': {dog_index_nobi2}")
