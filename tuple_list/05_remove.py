print("Tuple - list difference\n")
# tuple cannot have  elements modified, added, removed elements
# the methods can be visualised with: 
# - dir(tuple)  count 
# - dir(list)   count append extend insert remove pop clear sort reverse copy count 

# TUPLE IS AN IMUTABLE FIXED SIZE DATA STRUCTURE
# LIST IS AN MUTABLE VARIABLE SIZE DATA STRUCTURE - 

# In list but not in tuple
## avaible data can be modified, 
## new data can be added, 
## existent data removed

# insert
print("remove\n")

list_dogs = ['Spike', 'Nobi', 'Blakie']
dog_name = 'Nobi' 
print(f"List of dogs is {list_dogs}")

list_dogs.remove(dog_name) # remove first element with  value "Nobi"
print("="*50)
print(f"List of dogs after removal is {list_dogs}")
print("list.remove(value) - remove value if exist\nIf doesn't exist gives an error")

