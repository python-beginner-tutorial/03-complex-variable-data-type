print("Tuple - list difference\n")
# tuple cannot have  elements modified, added, removed elements
# the methods can be visualised with: 
# - dir(tuple)  count 
# - dir(list)   count append extend insert remove pop clear sort reverse copy count 

# TUPLE IS AN IMUTABLE FIXED SIZE DATA STRUCTURE
# LIST IS AN MUTABLE VARIABLE SIZE DATA STRUCTURE - 

# In list but not in tuple
## avaible data can be modified, 
## new data can be added, 
## existent data removed

# append
print("append\n")

ex_list = [2, 4, 8, 16, 32]
print(f"List before append {ex_list}")

# ex_tuple.append(64) # will cause an error 'tuple' object has no atribute 'append'

ex_list.append(64)  # will append value 64 to the end of the list
print(f"List after ex_list.append(64) {ex_list}")

