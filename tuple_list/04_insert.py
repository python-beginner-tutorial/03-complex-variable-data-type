print("Tuple - list difference\n")
# tuple cannot have  elements modified, added, removed elements
# the methods can be visualised with: 
# - dir(tuple)  count 
# - dir(list)   count append extend insert remove pop clear sort reverse copy count 

# TUPLE IS AN IMUTABLE FIXED SIZE DATA STRUCTURE
# LIST IS AN MUTABLE VARIABLE SIZE DATA STRUCTURE - 

# In list but not in tuple
## avaible data can be modified, 
## new data can be added, 
## existent data removed

# insert
print("insert\n")

separator = 50

list_dogs = ["Spike", "Nobi", "Blakie"]
print(f"List of dogs :{list_dogs}")

list_cats = ["Katty", "Rose", "Brownie" ]
print(f"List of cats: {list_cats}")
cat_name = "Chichi"

print("="*separator)
#list.insert(index, value) # 0 based index
print("Insert 'Milo' at index 1 in dog's list")
list_dogs.insert(1, "Milo") # ["Spike", "Milo" ,"Nobi", "Blakie"]
print("Insert Chichi at index 2 in cat's list")
list_cats.insert(2, cat_name)

print("="*separator)
print(list_dogs)
print(list_cats)
